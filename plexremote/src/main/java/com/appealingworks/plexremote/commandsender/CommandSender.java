/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.commandsender;

import com.appealingworks.plexremote.Shutdownable;

public interface CommandSender extends Shutdownable {

    public void setUseEscapeAsBack(boolean value);

    /**
     * Shuts down the HttpClient and the ExecutorService
     */
    public void shutdown();

    /**
     * @return true if shutdown has been called, otherwise false
     */
    public boolean isShutdown();

    public void up();

    public void down();

    public void left();

    public void right();

    public void select();

    public void back();

    public void play();

    public void pause();

    public void stop();

    public void skipPrevious();

    public void skipNext();

    public void bigStepForward();

    public void bigStepBack();

    public void stepForward();

    public void stepBack();

    public void rewind();

    public void fastForward();

    public void showMenu();

    public void showOSD();

    public void showCodecInfo();

    public void toggleSubtitle();

    public void subtitleLanguage();

    public void audioLanguage();

    public void setVolume(int volume);

    public void queue();

    public void sendText(String string);
}
