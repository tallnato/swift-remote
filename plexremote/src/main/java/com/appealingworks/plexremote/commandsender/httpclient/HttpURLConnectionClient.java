/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.commandsender.httpclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author Mikael Stockman
 */
public class HttpURLConnectionClient implements CommandHttpClient {
    private boolean mShutdown;

    public HttpURLConnectionClient() {
        mShutdown = false;
    }

    @Override
    public String execute(String myurl) throws ConnectionException,
            IllegalArgumentException {
        if (mShutdown)
            throw new IllegalStateException("shutdown() has been called");

        InputStream is = null;
        String content = null;
        try {
            HttpURLConnection conn = getConnection(myurl);

            execute(conn);
            is = conn.getInputStream();
            content = readStream(is);
            conn.disconnect();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                    is = null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return content;
    }

    @Override
    public void executeAndConsume(String myurl) throws ConnectionException,
            IllegalArgumentException {
        if (mShutdown)
            throw new IllegalStateException("shutdown() has been called");

        HttpURLConnection conn = getConnection(myurl);
        // Starts the query
        execute(conn);
        conn.disconnect();

    }

    /**
     * @param conn the HttpURLConnection
     * @throws ConnectionException if IOException
     */
    private void execute(HttpURLConnection conn) throws ConnectionException {
        try {
            conn.connect();
            // int responseCode =
            conn.getResponseCode();
            // if (responseCode / 100 != 2)
            // throw new ConnectionException("HttpRequest with responseCode: "
            // + responseCode);

        } catch (IOException e) {
            throw new ConnectionException(e.getMessage());
        }
    }

    /**
     * @param myurl the url
     * @return a HttpURLConnection object
     * @throws IllegalArgumentException if the url was invalid
     * @throws ConnectionException      if there was a problem opening the connection
     */
    private HttpURLConnection getConnection(String myurl)
            throws IllegalArgumentException, ConnectionException {
        HttpURLConnection conn;

        try {
            URL url = new URL(myurl);
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(1000);
            conn.setConnectTimeout(1500);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setUseCaches(false);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("The url: " + myurl
                    + " is not valid");
        } catch (IOException e) {
            throw new ConnectionException(e.getMessage());
        }

        return conn;
    }

    /**
     * @param in the InputStream
     * @return the content of the stream as a String
     * @throws java.io.IOException if there was a problem reading the stream
     */
    private String readStream(InputStream in) throws IOException {
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();

        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }

    @Override
    public void shutdown() {
        if (mShutdown)
            return;
        mShutdown = true;

    }

    @Override
    public boolean isShutdown() {
        return mShutdown;
    }
}
