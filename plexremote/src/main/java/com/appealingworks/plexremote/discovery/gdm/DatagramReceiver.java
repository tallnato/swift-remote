/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.discovery.gdm;

import com.appealingworks.plexremote.Shutdownable;
import com.appealingworks.plexremote.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class DatagramReceiver implements Shutdownable {
    private static final String TAG = DatagramReceiver.class.getSimpleName();
    private final DatagramPacketListener mDatagramPacketListener;
    private final Object mSyncObject;
    private DatagramSocket mDatagramSocket;
    private Thread mThread;
    // Make sure the mThread has the correct value
    private volatile boolean mShutdown;
    private final Runnable receiverRunnable = new Runnable() {

        @Override
        public void run() {

            byte[] buf = new byte[1024];
            DatagramPacket packet;
            while (!mShutdown) {

                packet = new DatagramPacket(buf, buf.length);

                Log.d(TAG, "Socket is now waiting for a packet");

                try {
                    mDatagramSocket.receive(packet);

                    Log.d(TAG, "Receved a packet from: "
                            + packet.getAddress().toString());

                    mDatagramPacketListener.onDatagramReceived(packet);

                } catch (IOException e) {
                    synchronized (mSyncObject) {
                        if (mDatagramSocket == null || mDatagramSocket.isClosed()) {

                            Log.d(TAG,
                                    "Sock has become null or closed after IOException");
                            break;
                        }
                    }
                    // Dont print stactrace if it was a result of a shutdown
                    if (!mShutdown) {
                        e.printStackTrace();
                    }
                }
            }

            // -------------------------------------------------------------

            Log.d(TAG, "Thread is now in absolute last code line");
        }
    };

    public DatagramReceiver(DatagramSocket datagramSocket,
                            DatagramPacketListener datagramPacketListener) {
        this.mShutdown = false;
        this.mDatagramSocket = datagramSocket;
        this.mDatagramPacketListener = datagramPacketListener;
        this.mThread = new Thread(receiverRunnable);
        mSyncObject = new Object();
    }

    public void start() {
        mThread.start();
    }

    public boolean isStarted() {
        return mThread.isAlive();
    }

    /**
     * Closes the provided socket
     */
    public void shutdown() {
        if (mShutdown)
            return;
        mShutdown = true;
        synchronized (mSyncObject) {
            if (mDatagramSocket != null) {
                mDatagramSocket.close();
                mDatagramSocket = null;
            }
        }
        if (mThread != null) {
            mThread.interrupt();

            boolean joined = false;

            while (!joined) {
                try {

                    Log.d(TAG,
                            "DatagramReceiver: trying to join reciver mThread");
                    mThread.join();
                    joined = true;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            Log.d(TAG, "DatagramReceiver: mThread is now joined");
            mThread = null;
        }
    }

    public boolean isShutdown() {
        return mShutdown;
    }
}
