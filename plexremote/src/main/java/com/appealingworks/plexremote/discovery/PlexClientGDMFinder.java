/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.discovery;

import com.appealingworks.plexremote.discovery.gdm.GDMFinder;
import com.appealingworks.plexremote.discovery.gdm.GDMHeadersParser;
import com.appealingworks.plexremote.discovery.gdm.GDMResponseListener;
import com.appealingworks.plexremote.discovery.gdm.GDMResponseMessage;
import com.appealingworks.plexremote.model.PlexClient;
import com.appealingworks.plexremote.util.Log;

import java.net.InetAddress;
import java.net.SocketException;
import java.util.Map;

/**
 * Searches for Plex Clients using the GDM protocol. Currently not used. Clients are found by
 * querying the selected Plex Server.
 */
public class PlexClientGDMFinder implements PlexFinder, GDMResponseListener {
    private static final String TAG = PlexClientGDMFinder.class.getSimpleName();
    private static final int GDM_PORT = 32412;
    private final GDMFinder mGdmFinder;
    private final PlexClientListener mPlexClientListener;
    private boolean mShutdown;

    /**
     * @param plexClientListener
     * @param gdmFinder
     * @throws java.net.SocketException
     */
    private PlexClientGDMFinder(PlexClientListener plexClientListener,
                                GDMFinder gdmFinder) throws SocketException {

        this.mGdmFinder = gdmFinder;
        this.mPlexClientListener = plexClientListener;
        this.mShutdown = false;

        mGdmFinder.setGdmResponseListener(this);
    }

    public static PlexClientGDMFinder newInstance(PlexClientListener plexClientListener, InetAddress broadcastAddress) throws SocketException {
        GDMFinder gdmFinder = new GDMFinder(broadcastAddress, GDM_PORT);
        return new PlexClientGDMFinder(plexClientListener, gdmFinder);
    }

    @Override
    public void shutdown() {
        if (mShutdown)
            return;
        mShutdown = true;

        if (mGdmFinder != null) {
            mGdmFinder.shutdown();
        }
    }

    @Override
    public boolean isShutdown() {
        return mShutdown;
    }

    @Override
    public void onGDMResponse(GDMResponseMessage response) {
        // If shutdown, don't do anything
        if (!mShutdown) {
            String content = response.getContent();

            if (content.contains("plex")) {

                Map<String, String> headers = GDMHeadersParser.parseHeaders(response);
                InetAddress address = response.getSource();
                PlexClient plexClient = PlexClient.parseFromHeaders(headers);
                plexClient.setAddress(address.getHostAddress());
                plexClient.setHost(address.getHostAddress());
                mPlexClientListener.onClientFound(plexClient);
            } else {
                Log.d(TAG, "Received msg not from plex client");
                Log.d(TAG, response.toString());
            }
        }
    }

    @Override
    public void find() {
        if (mShutdown) {
            throw new IllegalStateException("shutdown() has been called");
        }
        mGdmFinder.find();
    }
}
