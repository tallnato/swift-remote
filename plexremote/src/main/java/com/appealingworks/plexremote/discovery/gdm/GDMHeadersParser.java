/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.discovery.gdm;

import android.text.TextUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mikael Stockman on 03/01/14.
 */
public class GDMHeadersParser {
    public static Map<String, String> parseHeaders(GDMResponseMessage gdmResponseMessage) {
        String text = gdmResponseMessage.getContent();
        if (text == null) {
            return new HashMap<String, String>();
        }

        String[] lines = text.split("\n");
        Map<String, String> headers = new HashMap<String, String>(lines.length);

        try {
            for (String line : lines) {
                if (line.contains(":")) {
                    String[] splitString = line.split(":");
                    if (splitString.length == 2
                            && !TextUtils.isEmpty(splitString[0])
                            && !TextUtils.isEmpty(splitString[1])) {
                        headers.put(splitString[0].trim(), splitString[1].trim());
                    }
                }
            }
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        return headers;
    }
}
