/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.discovery.gdm;

import com.appealingworks.plexremote.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;

public class GDMFinder implements DatagramPacketListener {
    private static final String TAG = GDMFinder.class.getSimpleName();
    private static final String MSEARCH = "M-SEARCH * HTTP/1.1\r\n\r\n";
    private final Object mSyncObj;
    private final InetAddress mBroadcastAddress;
    private final int mGdmPort;
    private boolean mShutdown;
    private GDMResponseListener mGdmResponseListener;
    private DatagramReceiver mDatagramReceiver;
    private DatagramSocket mDatagramSocket;

    public GDMFinder(InetAddress broadcastAddress, int gdmPort) throws SocketException {

        this.mGdmPort = gdmPort;
        this.mBroadcastAddress = broadcastAddress;
        mSyncObj = new Object();

        init();
    }

    private void init() throws SocketException {
        try {
            mDatagramSocket = new DatagramSocket(null);
            mDatagramSocket.setReuseAddress(true);
            mDatagramSocket.setBroadcast(true);
            mDatagramSocket.bind(new InetSocketAddress(mGdmPort));

        } catch (SocketException e) {
            throw e;
        }

        mDatagramReceiver = new DatagramReceiver(mDatagramSocket, this);
    }

    public void find() {
        if (mShutdown) {
            throw new IllegalStateException("shutdown() has been called");
        }

        if (!mDatagramReceiver.isStarted())
            mDatagramReceiver.start();

        broadcastMSearchMessage();

        Log.d(TAG, "MSearch message has been broadcasted");

    }

    private void broadcastMSearchMessage() {
        // Should never actually happen since its private, and any enclosing
        // method should do this check first
        if (mShutdown) {
            throw new IllegalStateException("shutdown() has been called");
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (mSyncObj) {
                    try {
                        if (mBroadcastAddress != null) {

                            Log.d(TAG,
                                    "Broadcasting server request to ip: "
                                            + mBroadcastAddress.toString()
                            );

                            DatagramPacket packet = createMSearchPacket();

                            mDatagramSocket.send(packet);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    private DatagramPacket createMSearchPacket() {
        return new DatagramPacket(MSEARCH.getBytes(), MSEARCH.getBytes().length,
                mBroadcastAddress, mGdmPort);
    }

    public void shutdown() {
        synchronized (mSyncObj) {
            if (mShutdown)
                return;
            mShutdown = true;

            Log.d(TAG, "GDMFinder shutting down");

            if (mDatagramReceiver != null) {
                mDatagramReceiver.shutdown();
                mDatagramReceiver = null;
            }

            if (mDatagramSocket != null) {
                mDatagramSocket.close();
                mDatagramSocket = null;
            }
        }
    }

    public boolean isShutdown() {
        return mShutdown;
    }

    @Override
    public void onDatagramReceived(DatagramPacket datagramPacket) {
        // Do not care if we have been shut down
        if (!mShutdown) {
            String received = new String(datagramPacket.getData(), 0,
                    datagramPacket.getLength());
            Log.d(TAG, "Datagram: " + received);
            if (!received.equals(MSEARCH)) {
                mGdmResponseListener.onGDMResponse(new GDMResponseMessage(
                        received, datagramPacket.getAddress()));
            }
        }
    }

    public void setGdmResponseListener(GDMResponseListener gdmResponseListener) {
        mGdmResponseListener = gdmResponseListener;
    }
}
