/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appealingworks.plexremote.PlexRemoteApp;
import com.appealingworks.plexremote.R;
import com.appealingworks.plexremote.discovery.PlexFinder;
import com.appealingworks.plexremote.discovery.PlexServerGDMFinder;
import com.appealingworks.plexremote.discovery.PlexServerListener;
import com.appealingworks.plexremote.model.PlexClient;
import com.appealingworks.plexremote.model.PlexServer;
import com.appealingworks.plexremote.util.Log;
import com.appealingworks.plexremote.volley.requests.PlexClientRequest;

import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

public class AutoFindActivity extends AbstractFindActivity implements
        PlexServerListener {

    private static final String TAG = AutoFindActivity.class.getSimpleName();
    private final OnClickListener mManualButtonOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent myIntent = new Intent(v.getContext(),
                    ManualFindActivity.class);
            startActivity(myIntent);
        }
    };
    private final OnClickListener mProblemTextViewOnClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("http://appealingworks.wordpress.com/swift-remote-faq"));
            startActivity(intent);
        }

    };
    private PlexFinder mPlexServerFinder;
    private final List<PlexServer> mPlexServers = new ArrayList<>();
    private final List<PlexClient> mPlexClients = new ArrayList<>();
    private ArrayAdapter<PlexServer> mPlexServerAdapter;
    private Spinner mPlexServersSpinner;
    private Button mDoneButton;
    private ArrayAdapter<PlexClient> mPlexClientAdapter;
    /*
     * If there is an wifi connection but searchThread == null: initialize
     * searchThread and socket. If wifi connection and searchThread, search.
     *
     * If no Wifi connection but searchThread, hmm... socket should throw
     * exception??
     *
     * show no wifi information
     */
    private final OnClickListener mRefreshButtonOnClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            resetView();

            if (isWiFiConnected() || ignoreWiFiConnectionState()) {
                // Can happen if wifi was enabled after the activity was
                // started

                Log.d(TAG, "Creating PlexGDMFinder in mRefreshButtonOnClickListener");
                if (mPlexServerFinder == null) {
                    initPlexFinder();

                } else {
                    mPlexServerFinder.find();
                }
            } else {
                informAboutWifiState();
            }
        }
    };
    private final OnItemSelectedListener mServerSpinnerOnClickListener = new OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view,
                                   int position, long id) {
            if (!isFinishing()) {
                PlexServer plexServer = mPlexServers.get(mPlexServersSpinner.getSelectedItemPosition());
                requestClients(plexServer);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
        }
    };
    private Spinner mPlexClientsSpinner;
    /*
     * The activity is not longer visible, no reason to listen for answers.
     * Either the application is minimized, the user is changing wifi-settings,
     * the manual mode is chosen or the remotecontrol is used.
     *
     * If searchThread is searching, stop thread and close socket.
     */
    private final OnClickListener mDoneButtonOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {

            Object selectedServer = mPlexServersSpinner.getSelectedItem();
            Object selectedClient = mPlexClientsSpinner.getSelectedItem();

            if (selectedServer != null && selectedClient != null) {
                PlexServer server = mPlexServers.get(mPlexServersSpinner.getSelectedItemPosition());
                PlexClient client = mPlexClients.get(mPlexClientsSpinner.getSelectedItemPosition());

                PlexServer.storePlexServer(AutoFindActivity.this, server);

                Intent myIntent = RemoteActivity.getIntent(v.getContext(), server, client);
                startActivity(myIntent);
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autofind);

        mHandler = new Handler();

        PreferenceManager.setDefaultValues(this, R.xml.preferences, true);

        Log.d(TAG, "OnCreate");

        mPlexClientsSpinner = (Spinner) findViewById(R.id.spinner_clients);
        mDoneButton = (Button) findViewById(R.id.button_done);

        mPlexServersSpinner = (Spinner) findViewById(R.id.spinner_servers);

        Button refreshButton = (Button) findViewById(R.id.button_refresh);
        Button manualButton = (Button) findViewById(R.id.button_manual);
        refreshButton.setOnClickListener(mRefreshButtonOnClickListener);
        manualButton.setOnClickListener(mManualButtonOnClickListener);
        mDoneButton.setOnClickListener(mDoneButtonOnClickListener);

        mPlexServersSpinner.setOnItemSelectedListener(mServerSpinnerOnClickListener);

        mPlexServerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, mPlexServers);
        mPlexServerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mPlexServersSpinner.setAdapter(mPlexServerAdapter);

        mPlexClientAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, mPlexClients);
        mPlexClientAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mPlexClientsSpinner.setAdapter(mPlexClientAdapter);

        TextView problemsTextView = (TextView) findViewById(R.id.activity_autofind_problems);
        problemsTextView.setOnClickListener(mProblemTextViewOnClickListener);
    }

    private void initPlexFinder() {
        try {
            mPlexServerFinder = PlexServerGDMFinder.newInstance(this, getBroadcastAddress());
            mPlexServerFinder.find();
        } catch (SocketException e) {
            mPlexServerFinder = null;
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {

        Log.d(TAG, "onStop");

        if (mPlexServerFinder != null) {
            mPlexServerFinder.shutdown();
            mPlexServerFinder = null;
        }
        super.onStop();
    }

    /*
     * This is only called when the app is closed socket and searchThread are
     * already closed in onPause().
     */
    @Override
    protected void onDestroy() {

        Log.d(TAG, "OnDestroy");

        super.onDestroy();
    }

    /*
     * Either the app is just started or the user returns from another activity
     * such as wifi settings. Recreate mPlexServerFinder if a wifiConnection exists.
     */
    @Override
    protected void onStart() {
        super.onStart();

        Log.d(TAG, "onStart");

        resetView();

        if (isWiFiConnected() || ignoreWiFiConnectionState()) {
            initPlexFinder();
        }
    }

    private void requestClients(PlexServer plexServer) {
        PlexClientRequest request = new PlexClientRequest(plexServer, new Response.Listener<List<PlexClient>>() {
            @Override
            public void onResponse(List<PlexClient> clients) {
                mPlexClients.clear();
                if (clients != null) {
                    mPlexClients.addAll(clients);

                    if (mPlexClients.size() > 0) {
                        mDoneButton.setEnabled(true);
                    }
                    Log.i(TAG, "received clients: " + mPlexClients);
                }
                mPlexClientAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
            }
        }
        );
        request.setTag(this);
        PlexRemoteApp.getApp().getVolleyRequestQueue().add(request);
    }

    protected void resetClientSpinner() {
        mPlexClients.clear();
        mPlexClientAdapter.notifyDataSetChanged();
    }

    private void resetView() {
        resetClientSpinner();
        resetServerSpinner();
        mDoneButton.setEnabled(false);
    }

    private void resetServerSpinner() {
        mPlexServers.clear();
        mPlexServerAdapter.notifyDataSetChanged();
    }

    @Override
    public void onServerFound(PlexServer plexServer) {
        if (!isFinishing()) {
            if (!mPlexServers.contains(plexServer)) {
                mPlexServers.add(plexServer);

                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mPlexServerAdapter.notifyDataSetChanged();
                    }
                });
            }
        }
    }
}
