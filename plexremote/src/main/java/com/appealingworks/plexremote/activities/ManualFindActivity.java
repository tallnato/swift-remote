/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appealingworks.plexremote.PlexRemoteApp;
import com.appealingworks.plexremote.R;
import com.appealingworks.plexremote.model.PlexClient;
import com.appealingworks.plexremote.model.PlexServer;
import com.appealingworks.plexremote.util.Log;
import com.appealingworks.plexremote.volley.requests.PlexClientRequest;
import com.appealingworks.plexremote.volley.requests.PlexServerRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mikael Stockman
 */
public class ManualFindActivity extends AbstractFindActivity {

    private static final String TAG = ManualFindActivity.class.getSimpleName();
    
    private final List<PlexClient> mPlexClients = new ArrayList<>();

    private Button mDoneButton;
    private ArrayAdapter<PlexClient> mPlexClientAdapter;
    private Spinner mPlexClientsSpinner;

    /**
     * Contacts the provided server and retrieves a list of connected Plex
     * clients
     */
    private final OnClickListener mRequestClientsOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (isWiFiConnected() || ignoreWiFiConnectionState()) {
                resetView();
                try {
                    PlexServer plexServer = getPlexServerFromInputFields();
                    requestServerInfoAndClients(plexServer);
                } catch (NumberFormatException e) {
                    mStatusTextView.setText(getResources().getString(
                            R.string.invalidPortNumber));
                }
            } else {
                informAboutWifiState();
            }
        }

    };
    private final OnLongClickListener mRequestClientsButtonOnLongClickListener = new OnLongClickListener() {

        @Override
        public boolean onLongClick(View v) {
            PlexServer plexServer = getPlexServerFromInputFields();

            int selectedClientPosition = mPlexClientsSpinner.getSelectedItemPosition();
            PlexClient plexClient;
            if (selectedClientPosition != AdapterView.INVALID_POSITION) {
                plexClient = mPlexClients.get(selectedClientPosition);
            } else {
                plexClient = new PlexClient("", plexServer.getIpAddress());
            }
            Intent myIntent = RemoteActivity.getIntent(v.getContext(), plexServer, plexClient);
            startActivity(myIntent);
            return true;
        }
    };
    private final TextWatcher mSettingsChangeListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            resetView();
        }
    };
    private EditText mIpAddressEditText, mPortNumberEditText;
    private TextView mStatusTextView;
    private Button mRequestClientsButton;
    private PlexServer mPlexServer;
    private final OnClickListener mDoneButtonOnClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            PlexClient plexClient = mPlexClients.get(mPlexClientsSpinner.getSelectedItemPosition());

            Intent myIntent = RemoteActivity.getIntent(v.getContext(), mPlexServer, plexClient);
            startActivity(myIntent);
        }
    };

    /**
     * Called when the activity is first created.
     */
    @SuppressLint("ShowToast")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual);

        mIpAddressEditText = (EditText) findViewById(R.id.editText_ipAddress);
        mPortNumberEditText = (EditText) findViewById(R.id.editText_portNumber);
        mRequestClientsButton = (Button) findViewById(R.id.button_findClients);
        mStatusTextView = (TextView) findViewById(R.id.textView_status);
        mPlexClientsSpinner = (Spinner) findViewById(R.id.spinner_clients);
        mDoneButton = (Button) findViewById(R.id.button_done);

        mRequestClientsButton.setOnClickListener(mRequestClientsOnClickListener);
        mRequestClientsButton.setOnLongClickListener(mRequestClientsButtonOnLongClickListener);
        mDoneButton.setOnClickListener(mDoneButtonOnClickListener);

        mIpAddressEditText.addTextChangedListener(mSettingsChangeListener);
        mPortNumberEditText.addTextChangedListener(mSettingsChangeListener);

        mPlexClientAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, mPlexClients);
        mPlexClientAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mPlexClientsSpinner.setAdapter(mPlexClientAdapter);
    }

    private PlexServer getPlexServerFromInputFields() throws NumberFormatException {
        String serverIPAddress = mIpAddressEditText.getText().toString().trim();
        int serverPortNr = Integer.parseInt(mPortNumberEditText.getText().toString().trim());
        if (serverPortNr <= 0 || serverPortNr > 65535) {
            throw new NumberFormatException("Portnumber out of range");
        }

        PlexServer plexServer = new PlexServer();
        plexServer.setIpAddress(serverIPAddress);
        plexServer.setPortNr(serverPortNr);
        PlexServer.storePlexServer(ManualFindActivity.this, plexServer);
        return plexServer;
    }

    private void requestServerInfoAndClients(PlexServer plexServer) {
        mStatusTextView.setText(getResources()
                .getString(R.string.searching));
        mRequestClientsButton.setEnabled(false);

        PlexServerRequest request = new PlexServerRequest(plexServer, new Response.Listener<PlexServer>() {
            @Override
            public void onResponse(PlexServer plexServer) {
                mPlexServer = plexServer;
                requestClients(mPlexServer);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                mStatusTextView.setText(getResources().getString(
                        R.string.serverConnectionError));
                mRequestClientsButton.setEnabled(true);
            }
        });
        request.setTag(this);
        PlexRemoteApp.getApp().getVolleyRequestQueue().add(request);
    }

    private void requestClients(PlexServer plexServer) {
        mStatusTextView.setText(getResources()
                .getString(R.string.searching));
        mRequestClientsButton.setEnabled(false);

        PlexClientRequest request = new PlexClientRequest(plexServer, new Response.Listener<List<PlexClient>>() {
            @Override
            public void onResponse(List<PlexClient> clients) {
                if (clients != null) {
                    mPlexClients.addAll(clients);

                    if (mPlexClients.size() == 0) {
                        mDoneButton.setEnabled(false);
                        mStatusTextView.setText(getResources().getString(
                                R.string.noClientsFound));
                    } else {
                        mDoneButton.setEnabled(true);
                        if (mPlexClients.size() == 1) {
                            mStatusTextView.setText(getResources()
                                    .getString(R.string.oneClientFound));
                        } else {
                            mStatusTextView.setText(mPlexClients.size()
                                    + " " + getResources().getString(
                                    R.string.clientsFound));
                        }
                    }
                    Log.i(TAG, "received clients: " + clients);
                    mPlexClientAdapter.notifyDataSetChanged();
                    mRequestClientsButton.setEnabled(true);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                mStatusTextView.setText(getResources().getString(
                        R.string.serverConnectionError));
                mRequestClientsButton.setEnabled(true);
            }
        }
        );
        request.setTag(this);
        PlexRemoteApp.getApp().getVolleyRequestQueue().add(request);
    }

    private void loadSettings() {
        PlexServer plexServer = PlexServer.getStoredPlexServer(this);

        mIpAddressEditText.setText(plexServer.getIpAddress());
        mPortNumberEditText.setText(Integer.toString(plexServer.getPortNr()));
    }

    protected void resetClientSpinner() {
        mPlexClients.clear();
        mPlexClientAdapter.notifyDataSetChanged();
    }

    private void resetView() {
        resetClientSpinner();
        mDoneButton.setEnabled(false);
        mStatusTextView.setText("");
    }

    @SuppressLint("NewApi")
    @Override
    protected void onStart() {
        super.onStart();
        loadSettings();
        resetView();

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}
