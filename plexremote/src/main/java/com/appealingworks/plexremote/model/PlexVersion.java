/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.model;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mikael Stockman on 14/12/13.
 */
public class PlexVersion implements Comparable<PlexVersion>, Serializable {

    private String mVersion;

    public PlexVersion(String version) {
        mVersion = version;
    }

    private static int[] getVersionNumbers(String version) {
        if (version == null) {
            return new int[0];
        }

        String[] splitVersion = version.split("\\.");
        List<Integer> versionNumbers = new ArrayList<Integer>();
        try {
            for (String s : splitVersion) {
                if (s.contains("-")) {
                    versionNumbers.add(Integer.parseInt(s.substring(0, s.indexOf("-"))));
                } else {
                    versionNumbers.add(Integer.parseInt(s));
                }
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        int[] intVersionArray = new int[versionNumbers.size()];
        for (int i = 0; i < intVersionArray.length; i++) {
            intVersionArray[i] = versionNumbers.get(i);
        }

        return intVersionArray;
    }

    public String getVersionString() {
        return mVersion;
    }

    public void setVersionString(String version) {
        mVersion = version;
    }

    public boolean isGreaterOrEqualTo(String otherVersion) {
        return compareTo(new PlexVersion(otherVersion)) >= 0;
    }

    public boolean isGreaterOrEqualTo(PlexVersion otherVersion) {
        return compareTo(otherVersion) >= 0;
    }

    @Override
    public int compareTo(PlexVersion another) {
        if (another == null) {
            return 1;
        }

        int[] thisVersion = getVersionNumbers(getVersionString());
        int[] anotherVersion = getVersionNumbers(another.getVersionString());

        int countLimit = Math.min(thisVersion.length, anotherVersion.length);
        for (int i = 0; i < countLimit; i++) {
            if (thisVersion[i] != anotherVersion[i]) {
                return thisVersion[i] - anotherVersion[i];
            }
        }

        //If the lengths are equal, the versions are equal
        if (thisVersion.length == anotherVersion.length) {
            return 0;
        } else {
            //The longer version may be greater, unless the difference is extra zeroes
            if (thisVersion.length > anotherVersion.length) {
                for (int i = anotherVersion.length; i < thisVersion.length; i++) {
                    if (thisVersion[i] != 0) {
                        return 1;
                    }
                }
                return 0;
            } else {
                for (int i = thisVersion.length; i < anotherVersion.length; i++) {
                    if (anotherVersion[i] != 0) {
                        return -1;
                    }
                }
                return 0;
            }
        }
    }

    public static class PlexVersionDeserializer implements JsonDeserializer<PlexVersion> {
        @Override
        public PlexVersion deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException {
            String versionString = json.getAsString();
            return new PlexVersion(versionString);
        }
    }
}
