/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.model;

import android.content.Context;

import com.appealingworks.plexremote.R;
import com.google.gson.annotations.SerializedName;

public class PlexSection {

    @SerializedName("thumb")
    private String mThumb;

    @SerializedName("key")
    private int mKey;

    @SerializedName("type")
    private String mType;

    @SerializedName("title")
    private String mTitle;

    @SerializedName("refreshing")
    private int mRefreshing;

    @SerializedName("updatedAt")
    private long mUpdatedAt;

    @SerializedName("createdAt")
    private long mCreatedAt;

    public String getThumb() {
        return mThumb;
    }

    public void setThumb(String thumb) {
        this.mThumb = thumb;
    }

    public int getKey() {
        return mKey;
    }

    public void setKey(int key) {
        this.mKey = key;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        this.mType = type;
    }

    public String getDisplayTitle(Context context) {
        if (!isRefreshing()) {
            return getTitle();
        } else {
            return getTitle() + " (" + context.getString(R.string.refreshing) + ")";
        }
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public boolean isRefreshing() {
        return mRefreshing == 1;
    }

    public void setRefreshing(boolean refreshing) {
        this.mRefreshing = refreshing ? 1 : 0;
    }

    public long getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(long updatedAt) {
        this.mUpdatedAt = updatedAt;
    }

    public long getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(long createdAt) {
        this.mCreatedAt = createdAt;
    }

    @Override
    public String toString() {
        return mTitle;
    }
}
