/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.volley.requests;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.appealingworks.plexremote.model.PlexServer;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mikael Stockman on 07/12/13.
 */
public class PlexGsonRequest<T> extends GsonRequest<T> {

    private final Map<String, String> mHeaders;

    /**
     * Make a GET request and return a parsed object from JSON.
     *
     * @param plexServer    The plex server to send the request to
     * @param type          The type for Gson's reflection. e.g. new TypeToken<List<Model>>(){}.getType()
     * @param listener
     * @param errorListener
     */
    public PlexGsonRequest(PlexServer plexServer, String path, Type type, Response.Listener<T> listener, Response.ErrorListener errorListener) {
        this(plexServer, path, type, null, listener, errorListener);
    }

    /**
     * Make a GET request and return a parsed object from JSON.
     *
     * @param plexServer    The plex server to send the request to
     * @param type          The type for Gson's reflection. e.g. new TypeToken<List<Model>>(){}.getType()
     * @param headers       Map of request headers
     * @param listener
     * @param errorListener
     */
    public PlexGsonRequest(PlexServer plexServer, String path, Type type, Map<String, String> headers, Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(plexServer.getUrl() + path, type, listener, errorListener);
        mHeaders = new HashMap<String, String>();
        mHeaders.put("Content-Type", "application/json");
        mHeaders.put("Accept", "application/json");

        if (headers != null) {
            mHeaders.putAll(headers);
        }
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return mHeaders;
    }
}
