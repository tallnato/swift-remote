/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.volley.requests;

/**
 * Created by Mikael Stockman on 07/12/13.
 */

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.appealingworks.plexremote.util.Log;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;

/**
 * Volley adapter for JSON requests that will be parsed into Java objects by
 * Gson.
 */
public class GsonRequest<T> extends Request<T> {
    private static final String TAG = GsonRequest.class.getSimpleName();
    private final Gson mGson;
    private final Listener<T> mListener;
    private final Type mType;

    /**
     * Make a GET request and return a parsed object from JSON.
     *
     * @param url  URL of the request to make
     * @param type The type for Gson's reflection. e.g. new TypeToken<List<Model>>(){}.getType()
     */
    public GsonRequest(String url, Type type, Listener<T> listener, ErrorListener errorListener) {
        super(Method.GET, url, errorListener);
        mGson = createGson();
        mListener = listener;
        mType = type;
    }

    protected Gson createGson() {
        return new GsonBuilder().enableComplexMapKeySerialization().create();
    }

    @Override
    protected void deliverResponse(T response) {
        mListener.onResponse(response);
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse networkResponse) {
        try {
            String json = new String(networkResponse.data,
                    HttpHeaderParser.parseCharset(networkResponse.headers));
            Log.d(TAG, "Reponse: " + json);
            T parsedResponse = mGson.fromJson(json, mType);

            return Response.success(parsedResponse,
                    HttpHeaderParser.parseCacheHeaders(networkResponse));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }
}
