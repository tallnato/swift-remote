/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.graphics;

import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.util.StateSet;

/**
 * Created by miksto on 28/09/14.
 */
public class ButtonImageDrawable extends StateListDrawable {

    private ColorFilter mPressedColorFilter = new LightingColorFilter(Color.GRAY, 1);

    public ButtonImageDrawable(Drawable drawable) {
        super();
        addState(new int[]{android.R.attr.state_pressed}, drawable);
        addState(new int[]{android.R.attr.state_focused}, drawable);
        addState(new int[]{android.R.attr.state_selected}, drawable);
        addState(StateSet.WILD_CARD, drawable);
    }

    @Override
    protected boolean onStateChange(int[] stateSet) {

        boolean isPressed = false;

        for (int state : stateSet) {
            if (state == android.R.attr.state_pressed ||
                    state == android.R.attr.state_focused ||
                    state == android.R.attr.state_selected) {
                isPressed = true;
            }
        }

        if (isPressed) {
            setColorFilter(mPressedColorFilter);
        } else {
            clearColorFilter();
        }
        return super.onStateChange(stateSet);
    }
}
