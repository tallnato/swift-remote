/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.fragments;

import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.appealingworks.plexremote.util.Log;

public class JoystickTouchpadFragment extends AbstractTouchpadFragment {

    private static final float DP_THRESHOLD = 10;
    private static final long MIN_TIME_DELAY = 50;
    private static final long LONG_CLICK_DELAY = 200;
    private static final long MAX_INTER_COMMAND_DELAY = 700;
    private static final String KEY_USE_DOUBLE_TAP = "useDoubleTap";
    private final Movement mMovement = new Movement();
    private boolean mUseDoubleTap;
    private final GestureDetector mGestureDetector = new GestureDetector(getActivity(),
            new GestureDetector.SimpleOnGestureListener() {

                @Override
                public boolean onSingleTapConfirmed(MotionEvent e) {
                    if (mUseDoubleTap) {
                        Log.d(TAG, "onSingleTapConfirmed");
                        mCommandSender.select();
                        return true;
                    } else {
                        return false;
                    }
                }

                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    // If doubletap for back is not disabled, send select
                    // command right away.
                    if (!mUseDoubleTap) {
                        Log.d(TAG, "onSingleTapUp");
                        mCommandSender.select();
                        return true;
                    } else {
                        return false;
                    }
                }

                @Override
                public boolean onFling(MotionEvent e1, MotionEvent e2,
                                       float velocityX, float velocityY) {
                    Log.d(TAG, "Fling");
                    updateMovement(new Point(e1.getX(), e1.getY()), new Point(
                            e2.getX(), e2.getY()));
                    switch (mMovement.direction) {
                        case UP:
                            mCommandSender.up();
                            break;
                        case DOWN:
                            mCommandSender.down();
                            break;
                        case LEFT:
                            mCommandSender.left();
                            break;
                        case RIGHT:
                            mCommandSender.right();
                            break;
                    }
                    return false;
                }

                @Override
                public boolean onDoubleTap(MotionEvent e) {
                    if (mUseDoubleTap) {
                        mCommandSender.back();
                        return true;
                    } else {
                        return false;
                    }
                }
            }
    );
    private JoystickRunnable mJoystickRunnable;
    private LongClickRunnable mLongClickRunnable;
    private long mLastCommandExecutionTime;
    private long mLastCommandUpdateTime;
    private ScrollType mScrollType;
    private Point mFirstTouchDownEventPoint;
    private Point mPointForLastJoystickRunnableUpdate;
    private Point mPointForLastLongclickRunnableUpdate;

    public static JoystickTouchpadFragment newInstance(boolean useDoubleTap) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(KEY_USE_DOUBLE_TAP, useDoubleTap);
        JoystickTouchpadFragment fragment = new JoystickTouchpadFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            mUseDoubleTap = getArguments().getBoolean(KEY_USE_DOUBLE_TAP, true);
        } else {
            mUseDoubleTap = savedInstanceState.getBoolean(KEY_USE_DOUBLE_TAP, true);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(KEY_USE_DOUBLE_TAP, mUseDoubleTap);
        super.onSaveInstanceState(outState);
    }

    public void setUseDoubleTap(boolean useDoubleTap) {
        mUseDoubleTap = useDoubleTap;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Log.d(TAG, "Touch");
        mGestureDetector.onTouchEvent(event);

        final int action = event.getActionMasked();
        // ACTION_UP
        if (action == MotionEvent.ACTION_UP
                || action == MotionEvent.ACTION_CANCEL) {
            mHandler.removeCallbacks(mJoystickRunnable);
            mHandler.removeCallbacks(mLongClickRunnable);
            mJoystickRunnable = null;
            mLongClickRunnable = null;
            mScrollType = null;
            mFirstTouchDownEventPoint = null;

            // ACTION_DOWN
        } else if (action == MotionEvent.ACTION_DOWN) {

            mPointForLastJoystickRunnableUpdate = new Point(event.getX(),
                    event.getY());
            mPointForLastLongclickRunnableUpdate = new Point(event.getX(),
                    event.getY());

            mFirstTouchDownEventPoint = new Point(event.getX(), event.getY());

            // ACTION MOVE
        } else if (action == MotionEvent.ACTION_MOVE) {

            if (hasPassedEnoughTime()) {
                mLastCommandUpdateTime = System.currentTimeMillis();

                float mDpYDistSinceLastCommand = dpFromPx(mPointForLastJoystickRunnableUpdate.y
                        - event.getY());

                float mDpXDistSinceLastCommand = dpFromPx(mPointForLastJoystickRunnableUpdate.x
                        - event.getX());

                updateMovement(mFirstTouchDownEventPoint, new Point(
                        event.getX(), event.getY()));

                if (mJoystickRunnable != null) {

                    Direction newDirection = null;
                    if (mScrollType == ScrollType.HORIZONTAL) {
                        switch (mMovement.direction) {
                            case LEFT:
                                if (shouldUpdateRunnable(mDpXDistSinceLastCommand)) {
                                    newDirection = Direction.LEFT;
                                }
                                break;
                            case RIGHT:
                                if (shouldUpdateRunnable(mDpXDistSinceLastCommand)) {
                                    newDirection = Direction.RIGHT;
                                }
                                break;
                        }
                    } else if (mScrollType == ScrollType.VERTICAL) {

                        switch (mMovement.direction) {
                            case UP:
                                if (shouldUpdateRunnable(mDpYDistSinceLastCommand)) {
                                    newDirection = Direction.UP;
                                }
                                break;
                            case DOWN:
                                if (shouldUpdateRunnable(mDpYDistSinceLastCommand)) {
                                    newDirection = Direction.DOWN;
                                }
                                break;
                        }
                    }

                    if (newDirection != null) {

                        long currTime = System.currentTimeMillis();
                        mHandler.removeCallbacks(mJoystickRunnable);

                        mPointForLastJoystickRunnableUpdate = new Point(
                                event.getX(), event.getY());

                        int repeatDelay = getDelayFromDist();
                        mJoystickRunnable = new JoystickRunnable(newDirection,
                                repeatDelay);

                        long passedTimeSinceLastCommandExecution = (currTime - mLastCommandExecutionTime);
                        int initialDelay = (int) Math.max(repeatDelay
                                - passedTimeSinceLastCommandExecution, 0);

                        mHandler.postDelayed(mJoystickRunnable, initialDelay);

                    }
                } else {

                    Point currentPoint = new Point(event.getX(), event.getY());
                    double draggedDpDistance = getDpPointDistance(currentPoint,
                            mPointForLastLongclickRunnableUpdate);

                    if (draggedDpDistance > 20 || (draggedDpDistance > 10 && mLongClickRunnable == null)) {

                        Log.d(TAG, "Starting the longclick runnable with draggedDist: " + draggedDpDistance);
                        mPointForLastLongclickRunnableUpdate.x = currentPoint.x;
                        mPointForLastLongclickRunnableUpdate.y = currentPoint.y;

                        mHandler.removeCallbacks(mLongClickRunnable);
                        mLongClickRunnable = new LongClickRunnable(new Point(
                                event.getX(), event.getY()));
                        mHandler.postDelayed(mLongClickRunnable, LONG_CLICK_DELAY);
                    }
                }
            }
        }
        return true;
    }

    private boolean hasPassedEnoughTime() {
        return System.currentTimeMillis() - mLastCommandUpdateTime > MIN_TIME_DELAY;
    }

    private int getDelayFromDist() {
        float nr = (10000 / (mMovement.dpDistance));
        nr = Math.min(nr, MAX_INTER_COMMAND_DELAY);
        return (int) nr;
    }

    private double getDpPointDistance(Point p1, Point p2) {
        return dpFromPx((float) Math.sqrt(Math.pow(p1.x - p2.x, 2)
                + Math.pow(p1.y - p2.y, 2)));
    }

    private void updateMovement(Point e1, Point e2) {
        float yDiff = Math.abs(e1.y - e2.y);
        float xDiff = Math.abs(e1.x - e2.x);

        if (mScrollType == ScrollType.VERTICAL) {
            mMovement.dpDistance = dpFromPx(yDiff);

            if (e2.y > e1.y) {
                mMovement.direction = Direction.DOWN;
            } else {
                mMovement.direction = Direction.UP;
            }
        } else if (mScrollType == ScrollType.HORIZONTAL) {
            mMovement.dpDistance = dpFromPx(xDiff);
            if (e2.x > e1.x) {
                mMovement.direction = Direction.RIGHT;
            } else {
                mMovement.direction = Direction.LEFT;
            }
        } else {
            // Used before a hold is detected, and the scroll type is locked
            if (yDiff > xDiff) {
                mMovement.dpDistance = dpFromPx(yDiff);

                if (e2.y > e1.y) {
                    mMovement.direction = Direction.DOWN;
                } else {
                    mMovement.direction = Direction.UP;
                }
            } else {
                mMovement.dpDistance = dpFromPx(xDiff);
                if (e2.x > e1.x) {
                    mMovement.direction = Direction.RIGHT;
                } else {
                    mMovement.direction = Direction.LEFT;
                }
            }
        }
        Log.d(TAG, "MOVEMENT_DPDIST: " + mMovement.dpDistance);
    }

    private boolean shouldUpdateRunnable(float dpDistSinceLastCommand) {
        return Math.abs(dpDistSinceLastCommand) > DP_THRESHOLD;
    }

    private enum ScrollType {
        HORIZONTAL, VERTICAL
    }

    private static class Movement {
        Direction direction;
        float dpDistance;
    }

    private static class Point {
        float x;
        float y;

        public Point(float x, float y) {
            super();
            this.x = x;
            this.y = y;
        }
    }

    private class JoystickRunnable implements Runnable {
        final Direction direction;
        final long startTime;
        final int repeatDelay;

        public JoystickRunnable(Direction direction, int repeatDelay) {
            this.direction = direction;
            this.repeatDelay = repeatDelay;
            startTime = System.currentTimeMillis();
        }

        @Override
        public void run() {
            mLastCommandExecutionTime = System.currentTimeMillis();
            switch (direction) {
                case DOWN:
                    mCommandSender.down();
                    break;
                case UP:
                    mCommandSender.up();
                    break;
                case LEFT:
                    mCommandSender.left();
                    break;
                case RIGHT:
                    mCommandSender.right();
                    break;
            }
            mJoystickRunnable = new JoystickRunnable(direction, repeatDelay);
            mHandler.postDelayed(mJoystickRunnable, repeatDelay);
        }
    }

    /**
     * Runnable to detect a long hold on the touch pad. When a hold is detected,
     * the scrolling is locked to either horizontal or vertical.
     *
     * @author miksto
     */
    private class LongClickRunnable implements Runnable {

        final Point p;

        public LongClickRunnable(Point point) {
            this.p = point;
        }

        @Override
        public void run() {
            updateMovement(mFirstTouchDownEventPoint, p);
            int delay = getDelayFromDist();
            Direction direction = null;

            switch (mMovement.direction) {
                case UP:
                    direction = Direction.UP;
                    mScrollType = ScrollType.VERTICAL;
                    break;
                case DOWN:
                    direction = Direction.DOWN;
                    mScrollType = ScrollType.VERTICAL;
                    break;
                case LEFT:
                    direction = Direction.LEFT;
                    mScrollType = ScrollType.HORIZONTAL;
                    break;
                case RIGHT:
                    direction = Direction.RIGHT;
                    mScrollType = ScrollType.HORIZONTAL;
                    break;
            }
            Log.d("LongClickRunnable", "Initiating the repeatclicker");
            mHandler.removeCallbacks(mJoystickRunnable);
            mJoystickRunnable = new JoystickRunnable(direction, delay);
            mHandler.post(mJoystickRunnable);
        }
    }
}
