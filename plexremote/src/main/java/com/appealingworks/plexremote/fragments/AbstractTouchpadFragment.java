/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.appealingworks.plexremote.R;

public abstract class AbstractTouchpadFragment extends
        AbstractNavigationFragment implements View.OnTouchListener {

    protected Handler mHandler;
    protected float mScreenScale;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mScreenScale = getResources().getDisplayMetrics().density;
        mHandler = new Handler();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.component_touchpad, container, false);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.setOnTouchListener(this);
    }

    protected float dpFromPx(float px) {
        return px / mScreenScale;
    }

    private float pxFromDp(float dp) {
        return dp * mScreenScale;
    }

    protected enum Direction {
        DOWN, UP, LEFT, RIGHT
    }

}
