/*
 * Copyright 2014 Mikael Stockman
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.appealingworks.plexremote.util;

public class Constants {

	/* STORAGE NAMES */

    public static final String PREFS_VOLUME = "volume";
    public static final String PREFS_KEY_SHOW_NAVBUTTONS_HOW_TO = "shownavbuttonshowto";

    /* OTHERS */
    public static final String INCOMING_CALL_ACTION = "android.intent.action.PHONE_STATE";
    public static final String PREFS_KEY_HIDE_NOTIFICATION_BAR = "hideNotificationBar";
    public static final String PREFS_KEY_BACKBUTTON = "backButton";
    public static final String PREFS_KEY_BACKBUTTON_POS = "backButtonPos";
    public static final String PREFS_KEY_ALLOW_LANDSCAPE_MODE = "allowLandscapeMode";
    public static final String PREFS_KEY_TOUCHPAD = "touchPad";
    public static final String PREFS_KEY_KEEP_SCREEN_ON = "keepScreenOn";
    public static final String PREFS_KEY_USE_ESCAPE = "useEscape";
    public static final String PREFS_KEY_VERSION = "version";
    public static final String PREFS_KEY_AUTOPAUSE = "autopause";
    public static final String PREFS_KEY_DISABLE_WIFI_CHECK = "disableWiFiCheck";
    public static final String PREFS_KEY_DOUBLE_TAP_FOR_BACK = "doubleTapForBack";

    public static final String PREFS_KEY_SHOW_TOUCHPAD_HOW_TO = "showtouchpadhowto";
}
