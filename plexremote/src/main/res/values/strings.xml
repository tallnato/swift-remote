<?xml version="1.0" encoding="utf-8"?>
<!--
  ~ Copyright 2014 Mikael Stockman
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~     http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  -->

<resources>

    <string name="app_name">Swift Remote</string>

    <string name="manualTitle">Manual settings</string>
    <string name="ipInput">Server IP address</string>
    <string name="portNumberInput">Server port number (Default: 32400)</string>
    <string name="clientSpinner">Available plex players</string>
    <string name="findClientsButton">Find player</string>
    <string name="done">Done</string>
    <string name="autoFindHeader">Choose server and player</string>
    <string name="autoFindServerPicker">Choose server</string>
    <string name="autoFindClientPicker">Choose player</string>
    <string name="autoFindRefresh">Refresh</string>
    <string name="autoFindManual">Manual setup</string>
    <string name="navbuttons_howto_text">Press and hold any arrow button to scroll through the menus.\n\nUse the volume keys on the side of the phone to change the volume in Plex.\n\nThe round button in the middle works as enter.</string>
    <string name="touchpad_howto_text">A single swipe over the touchpad will take you one step up/down/left/right in plex.\n\nTo scroll through the menus, put a finger on the touchpad, drag it a little, and hold still for a short time, and the scrolling will start. The farther you drag, the faster the scrolling will be.\n\nIf you want to double tap the touchpad to go back in Plex, go to the settings, and check the option labeled "Touchpad double tap\n\nUse the volume keys on the side of the phone to change the volume in Plex."</string>
    <string name="navbuttons_howto_title">Using the navigation buttons!</string>
    <string name="touchpad_howto_title">Using the touchpad!</string>

    <string name="searching">Connecting&#8230;</string>
    <string name="serverConnectionError">Could not connect to the server.</string>
    <string name="noClientsFound">The Plex server returned 0 players.</string>
    <string name="clientsFound"> players returned.</string>
    <string name="oneClientFound">One player returned.</string>
    <string name="noWiFiMessage">You are not connected to a Wi-Fi network. For the remote to function, you need to be connected to the same network as the Plex server.</string>
    <string name="volume">Volume: </string>

    <string name="close">Close</string>
    <string name="noWiFiTitle">No Wi-Fi connection!</string>
    <string name="wiFiSettings">Wi-Fi settings</string>
    <string name="dontShowAgain">Don\'t show this again</string>
    <string name="showAgain">Show next time</string>
    <string name="refreshing">refreshing</string>
    <string name="noSections">Couldn\'t find any sections</string>
    <string name="invalidPortNumber">Invalid port number</string>

    <string name="remoteActivity_quit">Quit</string>

    <string name="menu_update_section">Refresh section</string>
    <string name="menu_settings">Settings</string>
    <string name="menu_subtitles_toggle">Toggle subtitle (s)</string>
    <string name="menu_subtitles_language">Subtitle language (l)</string>
    <string name="menu_codec_info">Codec information (i)</string>
    <string name="menu_audio_language">Audio language (a)</string>
    <string name="menu_osd">OSD (m)</string>
    <string name="menu_menu">Menu (c)</string>
    <string name="menu_queue">Queue (q)</string>

    <string name="prefs_category_features_title">Features</string>
    <string name="prefs_touchPad_summary">Use a touchpad for navigating in menus</string>
    <string name="prefs_touchPad_title">Touchpad</string>
    <string name="prefs_autopause_summary">Pause media on incoming call</string>
    <string name="prefs_autopause_title">Autopause</string>


    <string name="prefs_category_screen_title">Screen</string>
    <string name="prefs_keepScreenOn_summary">Do not turn off the screen due to inactivity</string>
    <string name="prefs_keepScreenOn_title">Keep the screen on</string>
    <string name="prefs_allowLandscapeMode_summary">Allow landscape mode on all screen sizes (The layout may break on too small screens)</string>
    <string name="prefs_allowLandscapeMode_title">Landscape mode</string>
    <string name="prefs_hideNotificationBar_summary">Hide the notification bar in the remote control</string>
    <string name="prefs_hideNotificationBar_title">Fullscreen mode</string>


    <string name="prefs_category_backbutton_title">Back button</string>
    <string name="prefs_use_android_back_button_summary">Use Android\'s back button to go back in Plex</string>
    <string name="prefs_use_android_back_button_title">Android\'s back button</string>
    <string name="prefs_backButton_pos_summary">Place the back button to the right</string>
    <string name="prefs_backButton_pos_title">Back button placement</string>
    <string name="prefs_useEscape_summary">Simulate the escape key when using the back button if this is supported by the plex player</string>
    <string name="prefs_useEscape_title">Simulate escape key</string>
    <string name="prefs_double_tap_to_go_back_summary">Double tap the touchpad to go back in Plex</string>
    <string name="prefs_double_tap_to_go_back_title">Touchpad double tap</string>

    <string name="prefs_category_other_settings_title">Other settings</string>
    <string name="prefs_disableWiFiCheck_summary">Do not require a Wi-Fi connection when searching for Plex servers/players</string>
    <string name="prefs_disableWiFiCheck_title">Disable Wi-Fi check</string>


    <string name="prefs_category_about_title">About</string>
    <string name="prefs_version_title">Version</string>

    <string name="url_plexremote_pro" translatable="false">market://details?id=com.appealingworks.plexremote.unlocker</string>
    <string name="activity_autofind_problems"><u>Problem? Check out the trouble shooting page!</u></string>
    <string name="choose_mode">Choose mode:</string>

</resources>
